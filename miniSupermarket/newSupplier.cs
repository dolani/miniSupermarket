﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miniSupermarket
{
    public partial class newSupplier : Form
    {
        database mydb;
        public newSupplier()
        {
            InitializeComponent();
            mydb = new database();
        }

        private void newSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show("You are here!!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name = textBoxBusinessName.Text;
            string address = textBoxBusinessAddress.Text;

            mydb.newSupplier(name,address);
        }

        private void newTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            transactions mytransact = new transactions();
            mytransact.Show();
        }

        private void newCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            categories mycat = new categories();
            mycat.Show();
        }

        private void newBuyerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            newBuyer mynewbuyer = new newBuyer();
            mynewbuyer.Show();
        }

        private void newSupplyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            supplies mysupplies = new supplies();
            mysupplies.Show();
        }

        private void newProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            products myproducts = new products();
            myproducts.Show();
        }

        private void newAttendantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            attendants myattendants = new attendants();
            myattendants.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 myform = new Form1();
            this.Hide();
            myform.Show();
        }
    }
}
