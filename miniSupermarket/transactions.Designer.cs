﻿namespace miniSupermarket
{
    partial class transactions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAttendantName = new System.Windows.Forms.Label();
            this.labelBuyerName = new System.Windows.Forms.Label();
            this.labeProductName = new System.Windows.Forms.Label();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.comboBoxAttendantsName = new System.Windows.Forms.ComboBox();
            this.comboBoxBuyersName = new System.Windows.Forms.ComboBox();
            this.comboBoxProductName = new System.Windows.Forms.ComboBox();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.btnEnter = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTransactionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.productsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suppliesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSupplyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxViewBuyersName = new System.Windows.Forms.TextBox();
            this.textBoxViewProductBought = new System.Windows.Forms.TextBox();
            this.textBoxViewQuantityBought = new System.Windows.Forms.TextBox();
            this.textBoxViewAmountPaid = new System.Windows.Forms.TextBox();
            this.textBoxViewAttendantsName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.newAttendantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelAttendantName
            // 
            this.labelAttendantName.AutoSize = true;
            this.labelAttendantName.Location = new System.Drawing.Point(31, 27);
            this.labelAttendantName.Name = "labelAttendantName";
            this.labelAttendantName.Size = new System.Drawing.Size(89, 13);
            this.labelAttendantName.TabIndex = 0;
            this.labelAttendantName.Text = "Attendants Name";
            // 
            // labelBuyerName
            // 
            this.labelBuyerName.AutoSize = true;
            this.labelBuyerName.Location = new System.Drawing.Point(31, 62);
            this.labelBuyerName.Name = "labelBuyerName";
            this.labelBuyerName.Size = new System.Drawing.Size(70, 13);
            this.labelBuyerName.TabIndex = 1;
            this.labelBuyerName.Text = "Buyers Name";
            // 
            // labeProductName
            // 
            this.labeProductName.AutoSize = true;
            this.labeProductName.Location = new System.Drawing.Point(31, 100);
            this.labeProductName.Name = "labeProductName";
            this.labeProductName.Size = new System.Drawing.Size(75, 13);
            this.labeProductName.TabIndex = 2;
            this.labeProductName.Text = "Product Name";
            // 
            // labelQuantity
            // 
            this.labelQuantity.AutoSize = true;
            this.labelQuantity.Location = new System.Drawing.Point(31, 130);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(46, 13);
            this.labelQuantity.TabIndex = 3;
            this.labelQuantity.Text = "Quantity";
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = true;
            this.labelAmount.Location = new System.Drawing.Point(31, 161);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(43, 13);
            this.labelAmount.TabIndex = 4;
            this.labelAmount.Text = "Amount";
            // 
            // comboBoxAttendantsName
            // 
            this.comboBoxAttendantsName.FormattingEnabled = true;
            this.comboBoxAttendantsName.Location = new System.Drawing.Point(148, 27);
            this.comboBoxAttendantsName.Name = "comboBoxAttendantsName";
            this.comboBoxAttendantsName.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAttendantsName.TabIndex = 5;
            // 
            // comboBoxBuyersName
            // 
            this.comboBoxBuyersName.FormattingEnabled = true;
            this.comboBoxBuyersName.Location = new System.Drawing.Point(147, 62);
            this.comboBoxBuyersName.Name = "comboBoxBuyersName";
            this.comboBoxBuyersName.Size = new System.Drawing.Size(121, 21);
            this.comboBoxBuyersName.TabIndex = 6;
            // 
            // comboBoxProductName
            // 
            this.comboBoxProductName.FormattingEnabled = true;
            this.comboBoxProductName.Location = new System.Drawing.Point(146, 100);
            this.comboBoxProductName.Name = "comboBoxProductName";
            this.comboBoxProductName.Size = new System.Drawing.Size(121, 21);
            this.comboBoxProductName.TabIndex = 7;
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.Location = new System.Drawing.Point(166, 130);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuantity.TabIndex = 8;
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(165, 161);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(100, 20);
            this.textBoxAmount.TabIndex = 9;
            // 
            // btnEnter
            // 
            this.btnEnter.Location = new System.Drawing.Point(225, 223);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(75, 23);
            this.btnEnter.TabIndex = 10;
            this.btnEnter.Text = "Enter";
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTransactionToolStripMenuItem,
            this.productsToolStripMenuItem,
            this.suppliesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(880, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newTransactionToolStripMenuItem
            // 
            this.newTransactionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.newTransactionToolStripMenuItem1,
            this.newAttendantsToolStripMenuItem});
            this.newTransactionToolStripMenuItem.Name = "newTransactionToolStripMenuItem";
            this.newTransactionToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.newTransactionToolStripMenuItem.Text = "Transactions";
            this.newTransactionToolStripMenuItem.Click += new System.EventHandler(this.newTransactionToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.newToolStripMenuItem.Text = "New Buyer";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // newTransactionToolStripMenuItem1
            // 
            this.newTransactionToolStripMenuItem1.Name = "newTransactionToolStripMenuItem1";
            this.newTransactionToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.newTransactionToolStripMenuItem1.Text = "New Transaction";
            this.newTransactionToolStripMenuItem1.Click += new System.EventHandler(this.newTransactionToolStripMenuItem1_Click);
            // 
            // productsToolStripMenuItem
            // 
            this.productsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem1,
            this.newProductToolStripMenuItem});
            this.productsToolStripMenuItem.Name = "productsToolStripMenuItem";
            this.productsToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.productsToolStripMenuItem.Text = "Products";
            // 
            // newToolStripMenuItem1
            // 
            this.newToolStripMenuItem1.Name = "newToolStripMenuItem1";
            this.newToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.newToolStripMenuItem1.Text = "New Category";
            this.newToolStripMenuItem1.Click += new System.EventHandler(this.newToolStripMenuItem1_Click);
            // 
            // newProductToolStripMenuItem
            // 
            this.newProductToolStripMenuItem.Name = "newProductToolStripMenuItem";
            this.newProductToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.newProductToolStripMenuItem.Text = "New Product";
            this.newProductToolStripMenuItem.Click += new System.EventHandler(this.newProductToolStripMenuItem_Click);
            // 
            // suppliesToolStripMenuItem
            // 
            this.suppliesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSupplierToolStripMenuItem,
            this.newSupplyToolStripMenuItem});
            this.suppliesToolStripMenuItem.Name = "suppliesToolStripMenuItem";
            this.suppliesToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.suppliesToolStripMenuItem.Text = "Supplies";
            // 
            // newSupplierToolStripMenuItem
            // 
            this.newSupplierToolStripMenuItem.Name = "newSupplierToolStripMenuItem";
            this.newSupplierToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.newSupplierToolStripMenuItem.Text = "New Supplier";
            this.newSupplierToolStripMenuItem.Click += new System.EventHandler(this.newSupplierToolStripMenuItem_Click);
            // 
            // newSupplyToolStripMenuItem
            // 
            this.newSupplyToolStripMenuItem.Name = "newSupplyToolStripMenuItem";
            this.newSupplyToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.newSupplyToolStripMenuItem.Text = "New Supply";
            this.newSupplyToolStripMenuItem.Click += new System.EventHandler(this.newSupplyToolStripMenuItem_Click);
            // 
            // textBoxViewBuyersName
            // 
            this.textBoxViewBuyersName.Location = new System.Drawing.Point(521, 63);
            this.textBoxViewBuyersName.Name = "textBoxViewBuyersName";
            this.textBoxViewBuyersName.ReadOnly = true;
            this.textBoxViewBuyersName.Size = new System.Drawing.Size(221, 20);
            this.textBoxViewBuyersName.TabIndex = 12;
            this.textBoxViewBuyersName.Visible = false;
            // 
            // textBoxViewProductBought
            // 
            this.textBoxViewProductBought.Location = new System.Drawing.Point(521, 101);
            this.textBoxViewProductBought.Name = "textBoxViewProductBought";
            this.textBoxViewProductBought.ReadOnly = true;
            this.textBoxViewProductBought.Size = new System.Drawing.Size(221, 20);
            this.textBoxViewProductBought.TabIndex = 13;
            this.textBoxViewProductBought.Visible = false;
            // 
            // textBoxViewQuantityBought
            // 
            this.textBoxViewQuantityBought.Location = new System.Drawing.Point(521, 130);
            this.textBoxViewQuantityBought.Name = "textBoxViewQuantityBought";
            this.textBoxViewQuantityBought.ReadOnly = true;
            this.textBoxViewQuantityBought.Size = new System.Drawing.Size(221, 20);
            this.textBoxViewQuantityBought.TabIndex = 14;
            this.textBoxViewQuantityBought.Visible = false;
            // 
            // textBoxViewAmountPaid
            // 
            this.textBoxViewAmountPaid.Location = new System.Drawing.Point(521, 161);
            this.textBoxViewAmountPaid.Name = "textBoxViewAmountPaid";
            this.textBoxViewAmountPaid.ReadOnly = true;
            this.textBoxViewAmountPaid.Size = new System.Drawing.Size(221, 20);
            this.textBoxViewAmountPaid.TabIndex = 15;
            this.textBoxViewAmountPaid.Visible = false;
            // 
            // textBoxViewAttendantsName
            // 
            this.textBoxViewAttendantsName.Location = new System.Drawing.Point(521, 28);
            this.textBoxViewAttendantsName.Name = "textBoxViewAttendantsName";
            this.textBoxViewAttendantsName.ReadOnly = true;
            this.textBoxViewAttendantsName.Size = new System.Drawing.Size(221, 20);
            this.textBoxViewAttendantsName.TabIndex = 16;
            this.textBoxViewAttendantsName.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(548, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(151, 25);
            this.button1.TabIndex = 17;
            this.button1.Text = "View Receipt";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // newAttendantsToolStripMenuItem
            // 
            this.newAttendantsToolStripMenuItem.Name = "newAttendantsToolStripMenuItem";
            this.newAttendantsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.newAttendantsToolStripMenuItem.Text = "New Attendant";
            this.newAttendantsToolStripMenuItem.Click += new System.EventHandler(this.newAttendantsToolStripMenuItem_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(581, 286);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Log Out";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // transactions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 368);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxViewAttendantsName);
            this.Controls.Add(this.textBoxViewAmountPaid);
            this.Controls.Add(this.textBoxViewQuantityBought);
            this.Controls.Add(this.textBoxViewProductBought);
            this.Controls.Add(this.textBoxViewBuyersName);
            this.Controls.Add(this.btnEnter);
            this.Controls.Add(this.textBoxAmount);
            this.Controls.Add(this.textBoxQuantity);
            this.Controls.Add(this.comboBoxProductName);
            this.Controls.Add(this.comboBoxBuyersName);
            this.Controls.Add(this.comboBoxAttendantsName);
            this.Controls.Add(this.labelAmount);
            this.Controls.Add(this.labelQuantity);
            this.Controls.Add(this.labeProductName);
            this.Controls.Add(this.labelBuyerName);
            this.Controls.Add(this.labelAttendantName);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "transactions";
            this.Text = "transactions";
            this.Load += new System.EventHandler(this.transactions_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAttendantName;
        private System.Windows.Forms.Label labelBuyerName;
        private System.Windows.Forms.Label labeProductName;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.Label labelAmount;
        private System.Windows.Forms.ComboBox comboBoxAttendantsName;
        private System.Windows.Forms.ComboBox comboBoxBuyersName;
        private System.Windows.Forms.ComboBox comboBoxProductName;
        private System.Windows.Forms.TextBox textBoxQuantity;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newTransactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTransactionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem productsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem suppliesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newSupplyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProductToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxViewBuyersName;
        private System.Windows.Forms.TextBox textBoxViewProductBought;
        private System.Windows.Forms.TextBox textBoxViewQuantityBought;
        private System.Windows.Forms.TextBox textBoxViewAmountPaid;
        private System.Windows.Forms.TextBox textBoxViewAttendantsName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem newAttendantsToolStripMenuItem;
        private System.Windows.Forms.Button button2;
    }
}