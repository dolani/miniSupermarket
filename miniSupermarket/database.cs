﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;


namespace miniSupermarket
{
    class database
    {
        string conn;
        MySqlConnection myconnect;
        MySqlCommand mycommand;
        MySqlDataAdapter myadapter;      
        public database()
        {
            conn = "SERVER=127.0.0.1;username=root;password=password;port=1234;database=minisupermarket";
            myconnect = new MySqlConnection(conn);
            mycommand = new MySqlCommand();
            myadapter = new MySqlDataAdapter();    
        }

        public void openconnection()
        {
            try
            {
                myconnect.Open();
            }
            catch(MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void transactions(int value,int value1,int value2,int quantity,int amount)
        {
            myconnect.Open();
            try
            {
                string query = "insert into transactions(attendant_id, buyer_id, product_id, quantity,amount) values (@value,@value1,@value2,@quantity,@amount)";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@value", value);
                mycommand.Parameters.AddWithValue("@value1", value1);
                mycommand.Parameters.AddWithValue("@value2", value2);
                mycommand.Parameters.AddWithValue("@quantity", quantity);
                mycommand.Parameters.AddWithValue("@amount", amount);

                mycommand.ExecuteNonQuery();
                MessageBox.Show("New Transaction Inserted Succesfully");
            }
            catch(MySqlException er){
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void newBuyer(string first_name)
        {
            try
            {
                myconnect.Open();

                string query = "insert into buyers (first_name) values (@name)";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@name", first_name);
                mycommand.ExecuteNonQuery();
                MessageBox.Show("New Buyer registered Successfully");
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void newSupplier(string name, string address)
        {
            try
            {
                myconnect.Open();
                string query = "insert into suppliers(business_name, business_address) values (@business_name,@business_address)";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@business_name",name);
                mycommand.Parameters.AddWithValue("@business_address", address);
                mycommand.ExecuteNonQuery();
                MessageBox.Show("New Supplier registered succesfully");
            }
            catch(MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void newCategory(string name)
        {
            try
            {
                myconnect.Open();
                string query = "insert into categories(name) values (@cat_name)";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@cat_name", name);
                mycommand.ExecuteNonQuery();
                MessageBox.Show("Categories Updated succesfully");
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            } 
        }

        public void supplies(int product_name,int supplier_name)
        {
            myconnect.Open();
            try
            {
                string query = "insert into supplies(product_id, supplier_id) values (@product_name,@supplier_name)";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@product_name", product_name);
                mycommand.Parameters.AddWithValue("@supplier_name", supplier_name);
                mycommand.ExecuteNonQuery();
                MessageBox.Show("New Supplies Updated Succesfully");
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void category(int category_name,string product_name,int price,string description)
        {
            myconnect.Open();
            try
            {
                string query = "insert into products(category_id,product_name, price,description) values (@category_name,@product_name,@price,@description)";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@category_name", category_name);
                mycommand.Parameters.AddWithValue("@product_name", product_name);
                mycommand.Parameters.AddWithValue("@price", price);
                mycommand.Parameters.AddWithValue("@description", description);

                mycommand.ExecuteNonQuery();
                MessageBox.Show("New Product Inserted Succesfully");
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void newAttendant(string name, string email, string phone, string address)
        {
            myconnect.Open();
            try
            {
                string query = "insert into attendants(fullname,email,phone_no,home_address) values (@name,@email,@phone,@address)";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@name",name);
                mycommand.Parameters.AddWithValue("@email",email);
                mycommand.Parameters.AddWithValue("@phone",phone);
                mycommand.Parameters.AddWithValue("@address",address);

                mycommand.ExecuteNonQuery();
                MessageBox.Show("New Attendant Addedd Successfully");
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }
    }
}
