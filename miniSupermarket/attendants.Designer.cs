﻿namespace miniSupermarket
{
    partial class attendants
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.tRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newBuyerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newAttendantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSupplyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAttendantName = new System.Windows.Forms.TextBox();
            this.textBoxAttendantEmail = new System.Windows.Forms.TextBox();
            this.textBoxAttendantPhone = new System.Windows.Forms.TextBox();
            this.textBoxAttendantAddress = new System.Windows.Forms.TextBox();
            this.btnAttendantSubmit = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tRToolStripMenuItem,
            this.productsToolStripMenuItem,
            this.supplToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(641, 24);
            this.menuStrip2.TabIndex = 2;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // tRToolStripMenuItem
            // 
            this.tRToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBuyerToolStripMenuItem,
            this.newTransactionToolStripMenuItem,
            this.newAttendantToolStripMenuItem});
            this.tRToolStripMenuItem.Name = "tRToolStripMenuItem";
            this.tRToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.tRToolStripMenuItem.Text = "Transactions";
            // 
            // newBuyerToolStripMenuItem
            // 
            this.newBuyerToolStripMenuItem.Name = "newBuyerToolStripMenuItem";
            this.newBuyerToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.newBuyerToolStripMenuItem.Text = "New Buyer";
            this.newBuyerToolStripMenuItem.Click += new System.EventHandler(this.newBuyerToolStripMenuItem_Click);
            // 
            // newTransactionToolStripMenuItem
            // 
            this.newTransactionToolStripMenuItem.Name = "newTransactionToolStripMenuItem";
            this.newTransactionToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.newTransactionToolStripMenuItem.Text = "New Transaction";
            this.newTransactionToolStripMenuItem.Click += new System.EventHandler(this.newTransactionToolStripMenuItem_Click);
            // 
            // newAttendantToolStripMenuItem
            // 
            this.newAttendantToolStripMenuItem.Name = "newAttendantToolStripMenuItem";
            this.newAttendantToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.newAttendantToolStripMenuItem.Text = "New Attendant";
            this.newAttendantToolStripMenuItem.Click += new System.EventHandler(this.newAttendantToolStripMenuItem_Click);
            // 
            // productsToolStripMenuItem
            // 
            this.productsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newCategoryToolStripMenuItem,
            this.newProductToolStripMenuItem});
            this.productsToolStripMenuItem.Name = "productsToolStripMenuItem";
            this.productsToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.productsToolStripMenuItem.Text = "Products";
            // 
            // newCategoryToolStripMenuItem
            // 
            this.newCategoryToolStripMenuItem.Name = "newCategoryToolStripMenuItem";
            this.newCategoryToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newCategoryToolStripMenuItem.Text = "New Category";
            this.newCategoryToolStripMenuItem.Click += new System.EventHandler(this.newCategoryToolStripMenuItem_Click);
            // 
            // newProductToolStripMenuItem
            // 
            this.newProductToolStripMenuItem.Name = "newProductToolStripMenuItem";
            this.newProductToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newProductToolStripMenuItem.Text = "New Product";
            this.newProductToolStripMenuItem.Click += new System.EventHandler(this.newProductToolStripMenuItem_Click);
            // 
            // supplToolStripMenuItem
            // 
            this.supplToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSupplierToolStripMenuItem,
            this.newSupplyToolStripMenuItem});
            this.supplToolStripMenuItem.Name = "supplToolStripMenuItem";
            this.supplToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.supplToolStripMenuItem.Text = "Supplies";
            // 
            // newSupplierToolStripMenuItem
            // 
            this.newSupplierToolStripMenuItem.Name = "newSupplierToolStripMenuItem";
            this.newSupplierToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newSupplierToolStripMenuItem.Text = "New Supplier";
            this.newSupplierToolStripMenuItem.Click += new System.EventHandler(this.newSupplierToolStripMenuItem_Click);
            // 
            // newSupplyToolStripMenuItem
            // 
            this.newSupplyToolStripMenuItem.Name = "newSupplyToolStripMenuItem";
            this.newSupplyToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newSupplyToolStripMenuItem.Text = "New Supply";
            this.newSupplyToolStripMenuItem.Click += new System.EventHandler(this.newSupplyToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Location = new System.Drawing.Point(62, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "E-mail";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Phone No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Home Address";
            // 
            // textBoxAttendantName
            // 
            this.textBoxAttendantName.Location = new System.Drawing.Point(186, 29);
            this.textBoxAttendantName.Name = "textBoxAttendantName";
            this.textBoxAttendantName.Size = new System.Drawing.Size(168, 20);
            this.textBoxAttendantName.TabIndex = 7;
            // 
            // textBoxAttendantEmail
            // 
            this.textBoxAttendantEmail.Location = new System.Drawing.Point(186, 92);
            this.textBoxAttendantEmail.Name = "textBoxAttendantEmail";
            this.textBoxAttendantEmail.Size = new System.Drawing.Size(200, 20);
            this.textBoxAttendantEmail.TabIndex = 8;
            // 
            // textBoxAttendantPhone
            // 
            this.textBoxAttendantPhone.Location = new System.Drawing.Point(186, 162);
            this.textBoxAttendantPhone.Name = "textBoxAttendantPhone";
            this.textBoxAttendantPhone.Size = new System.Drawing.Size(200, 20);
            this.textBoxAttendantPhone.TabIndex = 9;
            // 
            // textBoxAttendantAddress
            // 
            this.textBoxAttendantAddress.Location = new System.Drawing.Point(186, 215);
            this.textBoxAttendantAddress.Name = "textBoxAttendantAddress";
            this.textBoxAttendantAddress.Size = new System.Drawing.Size(200, 20);
            this.textBoxAttendantAddress.TabIndex = 10;
            // 
            // btnAttendantSubmit
            // 
            this.btnAttendantSubmit.Location = new System.Drawing.Point(320, 281);
            this.btnAttendantSubmit.Name = "btnAttendantSubmit";
            this.btnAttendantSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnAttendantSubmit.TabIndex = 11;
            this.btnAttendantSubmit.Text = "Submit";
            this.btnAttendantSubmit.UseVisualStyleBackColor = true;
            this.btnAttendantSubmit.Click += new System.EventHandler(this.btnAttendantSubmit_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(432, 281);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "Log Out";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // attendants
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 417);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnAttendantSubmit);
            this.Controls.Add(this.textBoxAttendantName);
            this.Controls.Add(this.textBoxAttendantAddress);
            this.Controls.Add(this.textBoxAttendantPhone);
            this.Controls.Add(this.textBoxAttendantEmail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip2);
            this.Name = "attendants";
            this.Text = "attendants";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem tRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newBuyerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTransactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newAttendantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newSupplyToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAttendantName;
        private System.Windows.Forms.TextBox textBoxAttendantEmail;
        private System.Windows.Forms.TextBox textBoxAttendantPhone;
        private System.Windows.Forms.TextBox textBoxAttendantAddress;
        private System.Windows.Forms.Button btnAttendantSubmit;
        private System.Windows.Forms.Button button2;
    }
}