﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace miniSupermarket
{
    public partial class Form1 : Form
    {
        string conn;
        database mydb;
        MySqlCommand mycommand;
        MySqlConnection myconnect;
        public Form1()
        {
            InitializeComponent();
            conn = "SERVER=127.0.0.1;username=root;password=password;port=1234;database=minisupermarket";
            mydb = new database();
            mycommand = new MySqlCommand();
           myconnect = new MySqlConnection(conn);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            mydb.openconnection();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            myconnect.Open();
            try
            {
                string emails = textBoxLoginEmail.Text;
                string query = "select * from attendants where email = '" + emails + "';";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                MySqlDataReader myreader;

                myreader = mycommand.ExecuteReader();
                int count = 0;
                while (myreader.Read())
                {
                    count++;
                }
                if (count == 1)
                {
                this.Hide();
                transactions mytransact = new transactions();
                mytransact.Show();

                }
                else
                {
                    MessageBox.Show("user email does not exist");
                }
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();

            }
            
        }
    }
}
